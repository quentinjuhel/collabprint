# Collabprint

Collabprint is a fork of [ether2html](http://osp.kitchen/tools/ether2html/) made by Open Source Publishing.

*Working document in the browser*

![](http://osp.kitchen/api/http://osp.kitchen/api/osp.tools.ether2html/45f025c0b37efd149d4dfdb9ce0af2d01ff5ff81/blob-data/book-screengrab.png)

# How to use it

1. Create an etherpad somewhere for the CSS (e.g. <https://framapad.org>).
2. Create an etherpad somewhere for the content (e.g. <https://framapad.org>).
3. Click on the link [ether2html.html](http://osp.kitchen/tools/ether2html/tree/master/ether2html.html#project-detail-files) then download the file by right-click "Download raw" and choose "Save link as".
4. Edit that `ether2html.html` file by replacing the URL under the comment `<!-- CHANGE THE URL OF YOUR CSS PAD BELOW -->` by the export URL of the pad CSS you created in step 1, copy the link location of the plain text export of the pad - see below the screen capture.
5. Edit that `ether2html.html` file by replacing the URL under the comment `<!-- CHANGE THE URL OF YOUR MARKDOWN CONTENT PAD BELOW -->` by the export URL of the pad for the content you created in step 2, copy the link location of the plain text export of the pad - see below the screen capture.
6. Open the file `ether2html.html` in your web browser (Firefox or Chrome).
7. Edit your pad with content (markdown or html) and your pad with CSS styles. 
8. Reload the file `ether2html.html` opened in your web browser. 
9. Use the print function of your browser and choose "Save as file". Here is your pdf ready to print!

*Copy the link location of your pad*

![](http://osp.kitchen/api/osp.tools.ether2html/b72c830156ff61069478b0052f9d77b52affa539/blob-data/pad-menu-screengrab.png)

# Markdown

Showdown.js parse markdown code with some specificities, look at [https://github.com/showdownjs/showdown/wiki/Showdown's-Markdown-syntax#headings](https://github.com/showdownjs/showdown/wiki/Showdown's-Markdown-syntax#headings). For next versions or workshops we would maybe explore the available options at [https://github.com/showdownjs/showdown/wiki/Showdown-Options](https://github.com/showdownjs/showdown/wiki/Showdown-Options).

# Licence

© Quentin Juhel under the GNU-GPL3
